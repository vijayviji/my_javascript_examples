var p = [];

p[0] = new Promise(function (resolve, reject) {
  setTimeout(function () {
    console.log("Resolving first promise");
    resolve(1);
  }, 5000);
});

p[1] = new Promise(function (resolve, reject) {
  setTimeout(function () {
    console.log("Resolving second promise");
    resolve(2);
  }, 10000);
});

p[2] = new Promise(function (resolve, reject) {
  setTimeout(function () {
    console.log("Resolving third promise");
    resolve(3);
  }, 13000);
});

Promise.all(p).then(function (values) {
  console.log("Resolving everything");
  console.log(values);
});
